package com.github.axet.torrentclient.dialogs;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.webkit.WebView;

import com.github.axet.androidlibrary.net.HttpClient;
import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.androidlibrary.widgets.WebViewCustom;
import com.github.axet.torrentclient.activities.MainActivity;
import com.github.axet.torrentclient.app.Storage;

public class BrowserDialogFragment extends com.github.axet.androidlibrary.widgets.BrowserDialogFragment implements MainActivity.TorrentFragmentInterface {
    public static String TAG = BrowserDialogFragment.class.getSimpleName();

    ViewPager pager;
    Thread thread;

    public static BrowserDialogFragment create(String head, String url, String cookies, String js, String js_post) {
        BrowserDialogFragment f = new BrowserDialogFragment();
        Bundle args = new Bundle();
        args.putString("head", head);
        args.putString("url", url);
        args.putString("js", js);
        args.putString("js_post", js_post);
        args.putString("cookies", cookies);
        f.setArguments(args);
        return f;
    }

    public static BrowserDialogFragment createHtml(String html_base, String head, String html, String js, String js_post) {
        BrowserDialogFragment f = new BrowserDialogFragment();
        Bundle args = new Bundle();
        args.putString("base", html_base);
        args.putString("head", head);
        args.putString("html", html);
        args.putString("js", js);
        args.putString("js_post", js_post);
        f.setArguments(args);
        return f;
    }

    public BrowserDialogFragment() {
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        if (web != null) {
            web.destroy();
            web = null;
        }

        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(result);
        }
    }

    @Override
    public void update() {
        // dialog maybe created but onCreateView not yet called
        if (pager == null)
            return;
    }

    @Override
    public void close() {
    }

    MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.startsWith(Storage.SCHEME_MAGNET)) {
            if (Storage.magnetName(url) == null)
                url = Storage.magnetName(url, view.getTitle());
            getMainActivity().addMagnet(url, getArguments().getString("url"));
            return true;
        }
        return false;
    }

    @Override
    public boolean onDownloadStart(final String base, final String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
        Log.d(TAG, "onDownloadStart " + url);
        final MainActivity main = getMainActivity();
        thread = new Thread("Browser Download File") {
            @Override
            public void run() {
                try {
                    final byte[] buf = http.getBytes(base, url);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (main.isFinishing())
                                return;
                            main.addTorrentFromBytes(buf);
                        }
                    });
                } catch (RuntimeException e) {
                    WebViewCustom.logIO(url, e);
                    web.load(WebViewCustom.ABOUT_ERROR, new HttpClient.HttpError(url, e));
                }
            }
        };
        thread.start();
        return true;
    }

    @Override
    public void onErrorMessage(String msg) {
        ErrorDialog.Post(getActivity(), msg);
    }
}
